import requests
import json
from datetime import datetime


def get_exchange_rates():
    # Встановлюємо URL API НБУ та параметри запиту
    url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
    params = {"date": datetime.now().strftime("%Y%m%d")}

    # Виконуємо запит до API НБУ та перевіряємо статус-код відповіді
    response = requests.get(url, params=params)
    if response.status_code != 200:
        raise Exception(f"Помилка отримання курсу валют з API НБУ: {response.status_code}")

    # Розпаковуємо відповідь та перевіряємо, що курси валют наявні та не порожні
    exchange_rates = response.json()
    if not exchange_rates:
        raise Exception("Курси валют не знайдені в API НБУ")

    # Формуємо рядок з датою створення запиту
    request_date = datetime.now().strftime("%d.%m.%Y %H:%M:%S")

    # Формуємо список з курсами валют
    exchange_rates_list = [f"{request_date}\n"]
    for rate in exchange_rates:
        # Перевіряємо, що назва та курс валюти наявні та не порожні
        if not rate.get("cc") or not rate.get("rate"):
            raise Exception(f"Невірний формат курсу валюти: {rate}")
        exchange_rates_list.append(f"{rate['cc']} to UAH: {rate['rate']}\n")

    # Записуємо список з курсами валют до файлу та перевіряємо, що запис було успішним
    filename = "exchange_rates.txt"
    with open(filename, "w") as f:
        f.writelines(exchange_rates_list)
    with open(filename, "r") as f:
        if f.read() != "".join(exchange_rates_list):
            raise Exception(f"Помилка запису курсів валют до файлу: {filename}")

if __name__ == "__main__":
    try:
        get_exchange_rates()
        print("Курси валют успішно записано до файлу exchange_rates.txt")
    except Exception as e:
        print(f"Помилка: {e}")
