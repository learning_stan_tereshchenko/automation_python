import sys
sys.path.append('..')
import pytest
from selenium import webdriver
from pages.radio_button_page import RadioButtonPage


class TestRadioButton:

    @pytest.fixture(scope="class")
    def radio_button_page(self, request, browser):
        page = RadioButtonPage(browser)
        page.load()
        def fin():
            browser.quit()
        request.addfinalizer(fin)
        return page

    def test_activate_yes_radio(self, radio_button_page):
        # Активация кнопки "Yes" и проверка ее состояния двумя разными способами
        radio_button_page.activate_radio_button('Yes')
        assert radio_button_page.is_radio_button_selected('Yes')
        assert radio_button_page.get_selected_radio_button() == 'Yes'

    def test_get_radio_buttons_info(self, radio_button_page):
        # Получение информации о радио-кнопках на странице
        radio_buttons = radio_button_page.get_radio_buttons_info()
        assert len(radio_buttons) == 2
        assert radio_buttons[0]['name'] == 'Yes'
        assert radio_buttons[0]['enabled'] is True
        assert radio_buttons[0]['selected'] is False
        assert radio_buttons[1]['name'] == 'Impressive'
        assert radio_buttons[1]['enabled'] is False
        assert radio_buttons[1]['selected'] is False

    def test_activate_disabled_radio_button(self, radio_button_page):
        # Активация и проверка состояния неактивной кнопки "No"
        radio_button_page.activate_radio_button('No')
        assert radio_button_page.is_radio_button_enabled('No')
        assert radio_button_page.is_radio_button_selected('No')
