import sys
sys.path.append('..')
from selenium import webdriver
import pytest
from pages.checkbox_page import CheckboxPage


class TestCheckbox:
    @pytest.fixture(scope='function')
    def checkbox_page(self):
        driver = webdriver.Chrome()
        page = CheckboxPage(driver)
        yield page
        driver.quit()

    def test_checkboxes(self, checkbox_page):
        expected_checkboxes = [
            'Commands',
            'Documents',
            'Office',
        ]
        checkbox_page.go_to()
        checkbox_page.check_multiple_checkboxes(expected_checkboxes)
        checkboxes = checkbox_page.get_checked_checkboxes()
        assert checkboxes == expected_checkboxes
