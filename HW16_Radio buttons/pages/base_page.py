from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class BasePage:
    """Базовый класс для всех страниц приложения."""

    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://demoqa.com/"
        self.title = ""

    def wait_for_element(self, locator, timeout=10):
        """Ожидание элемента на странице"""
        wait = WebDriverWait(self.driver, timeout)
        wait.until(EC.presence_of_element_located(locator))

    def wait_for_title(self, title, timeout=10):
        """Ожидание заголовка страницы"""
        wait = WebDriverWait(self.driver, timeout)
        wait.until(EC.title_contains(title))

    def go_to_page(self, url):
        """Переход на указанную страницу"""
        self.driver.get(self.base_url + url)
        self.wait_for_title(self.title)

    def go_to_home_page(self):
        """Переход на главную страницу"""
        self.go_to_page("")

    def go_to_checkboxes_page(self):
        """Переход на страницу с чекбоксами"""
        self.go_to_page("checkbox")

    def go_to_radio_button_page(self):
        """Переход на страницу с радио-кнопками"""
        self.go_to_page("radio-button")

    def get_current_url(self):
        """Получение текущего URL"""
        return self.driver.current_url

    def get_title(self):
        """Получение заголовка страницы"""
        return self.driver.title
