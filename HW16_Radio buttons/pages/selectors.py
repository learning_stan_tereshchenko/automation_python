class RadioButtonSelectors:
    RADIO_BUTTONS = "input[type='radio']"
    YES_RADIO_BUTTON = "#yesRadio"
    NO_RADIO_BUTTON = "#noRadio"
class CheckboxPageSelectors:
    CHECKBOX_SELECTOR = 'input[type="checkbox"]'
    BUTTON_SELECTOR = 'button[type="submit"]'