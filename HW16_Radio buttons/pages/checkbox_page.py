import sys
print(sys.path)
from selenium.webdriver.common.by import By
from .base_page import BasePage
from .selectors import CheckboxPageSelectors

class CheckboxPage(BasePage):
    """Класс для работы со страницей https://demoqa.com/checkbox"""

    def __init__(self, driver):
        super().__init__(driver)
        self.url = 'https://demoqa.com/checkbox'
        self.selectors = CheckboxPageSelectors

    def select_checkboxes(self, *checkbox_names):
        """Выбор чекбоксов по их именам"""
        for checkbox_name in checkbox_names:
            checkbox_selector = self.selectors.CHECKBOX_BY_LABEL.format(checkbox_name)
            checkbox = self.find_element((By.CSS_SELECTOR, checkbox_selector))
            if not checkbox.is_selected():
                checkbox.click()
