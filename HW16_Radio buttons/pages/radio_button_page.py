import sys
print(sys.path)
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from .base_page import BasePage


class RadioButtonPage(BasePage):
    # Locators
    RADIO_BUTTONS = (By.XPATH, "//input[@type='radio']")
    YES_RADIO_BUTTON = (By.ID, "yesRadio")
    NO_RADIO_BUTTON = (By.ID, "noRadio")

    # Web elements
    @property
    def radio_buttons(self) -> WebElement:
        return self.driver.find_elements(*self.RADIO_BUTTONS)

    @property
    def yes_radio_button(self) -> WebElement:
        return self.driver.find_element(*self.YES_RADIO_BUTTON)

    @property
    def no_radio_button(self) -> WebElement:
        return self.driver.find_element(*self.NO_RADIO_BUTTON)

    # Methods
    def get_radio_buttons_info(self) -> dict:
        """
        Returns a dictionary with information about the radio buttons on the page:
        {name: {'checked': bool, 'enabled': bool}}
        """
        info_dict = {}
        for radio in self.radio_buttons:
            name = radio.get_attribute('name')
            if name not in info_dict:
                info_dict[name] = {'checked': False, 'enabled': False}
            if radio.is_selected():
                info_dict[name]['checked'] = True
            if radio.is_enabled():
                info_dict[name]['enabled'] = True
        return info_dict

    def activate_yes_radio_button(self):
        self.yes_radio_button.click()

    def activate_disabled_radio_button(self):
        self.no_radio_button.click()
