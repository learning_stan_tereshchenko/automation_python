Список команд для тестирования страницы https://demoqa.com
test_checkboxes - тест для проверки работы чекбоксов на странице https://demoqa.com/checkbox
test_activate_yes_radio - тест для проверки активации кнопки "Yes" на странице https://demoqa.com/radio-button
test_get_radio_buttons_info - тест для получения информации о радио-кнопках на странице https://demoqa.com/radio-button
test_activate_disabled_radio_button - бонус-тест для активации отключенной кнопки "No" на странице https://demoqa.com/radio-button