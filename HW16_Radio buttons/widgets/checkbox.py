from selenium.webdriver.common.by import By

class CheckboxPage:
    def __init__(self, driver):
        self.driver = driver
        self.checkboxes = {
            "Commands": (By.XPATH, "//span[text()='Commands']/ancestor::label/input"),
            "General": (By.XPATH, "//span[text()='General']/ancestor::label/input"),
            "Home": (By.XPATH, "//span[text()='Home']/ancestor::label/input"),
            "Desktop": (By.XPATH, "//span[text()='Desktop']/ancestor::label/input"),
            "Documents": (By.XPATH, "//span[text()='Documents']/ancestor::label/input"),
            "Office": (By.XPATH, "//span[text()='Office']/ancestor::label/input"),
        }

    def select_checkboxes(self, checkbox_names):
        for checkbox_name in checkbox_names:
            checkbox = self.checkboxes[checkbox_name]
            checkbox_element = self.driver.find_element(*checkbox)
            if not checkbox_element.is_selected():
                checkbox_element.click()
