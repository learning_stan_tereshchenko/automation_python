from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class RadioButton:
    def __init__(self, driver):
        self.driver = driver
        self.radio_buttons = {
            "Yes": {"locator": (By.ID, "yesRadio"), "selected": False},
            "Impressive": {"locator": (By.ID, "impressiveRadio"), "selected": False},
            "No": {"locator": (By.ID, "noRadio"), "selected": False},
        }

    def activate_radio_button(self, button_name):
        button = self.radio_buttons[button_name]
        element = self.driver.find_element(*button["locator"])
        if not button["selected"]:
            element.click()
            button["selected"] = True

    def get_radio_buttons_info(self):
        info = {}
        for name, button in self.radio_buttons.items():
            element = self.driver.find_element(*button["locator"])
            info[name] = {
                "selected": button["selected"],
                "activated": EC.element_to_be_clickable(button["locator"])(self.driver),
            }
        return info
