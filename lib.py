# Напишіть декоратор, який визначає час виконання функції. Заміряйте час виконання функцій
import time

def timer(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f"Час виконання функції {func.__name__}: {end_time - start_time} секунд")
        return result
    return wrapper
#1 Напишіть функцію, яка визначає сезон за датою.
# Функція отримує стрінг у форматі "[день].[місяць]" (наприклад "12.01", "30.08", "1.11" і тд)
# і повинна повернути стрінг з відповідним сезоном, до якого відноситься ця дата ("літо", "осінь", "зима", "весна")
@timer
def get_season(date_str):
    """Повертає сезон на основі дати.

    Аргументи:
    date_str (str): Рядок у форматі "[день].[місяць]", наприклад "21.04", "01.12", і т.д.

    Повертає:
    str: Рядок з відповідним сезоном ("весна", "літо", "осінь" або "зима").
    """
    day_month = date_str.split('.')
    month = int(day_month[1])
    if month in range(3, 6):
        return "весна"
    elif month in range(6, 9):
        return "літо"
    elif month in range(9, 12):
        return "осінь"
    else:
        return "зима"
# Наприклад мій день народження
date_str = "21.04"
season = get_season(date_str)
print(season)

#2 'Тупий калькулятор'
@timer
def calculator(a, b, op):
    """
        Повертає результат арифметичної операції над двома числами, де операція задається рядком.

        Аргументи:
        a (int або float): перше число для обчислення
        b (int або float): друге число для обчислення
        op (str): операція, яку потрібно виконати. Допустимі значення: "+", "-", "*", "/"

        Повертає:
        int або float: результат арифметичної операції
        None: якщо або a або b має невірний тип даних або op не є допустимою операцією
        """
    if type(a) != int and type(a) != float:
        print("Невірний тип даних")
        return None
    if type(b) != int and type(b) != float:
        print("Невірний тип даних")
        return None

    if op == '+':
        return a + b
    elif op == '-':
        return a - b
    elif op == '*':
        return a * b
    elif op == '/':
        if b == 0:
            print("Ділення на нуль неможливе")
            return None
        return a / b
    else:
        print("Операція не підтримується")
        return None
result = calculator(3, 4, '+')
print(result)  # Плюс

result = calculator(8, 2, '-')
print(result)  # Мінус

result = calculator(10, 2, '/')
print(result)  # Ділення

result = calculator(3, 0, '/')
print(result)  # виводить "Ділення на нуль неможливе"

result = calculator(2, 2, '*')
print(result)  # Множення

result = calculator(2, "abc", '*')
print(result)  # виводить "Невірний тип даних"

result = calculator(4, 5, '&')
print(result)  # виводить "Операція не підтримується"

