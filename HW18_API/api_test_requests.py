import requests

def get_cat_fact():
    response = requests.get('https://cat-fact.herokuapp.com/facts/random')
    if response.status_code == 200:
        data = response.json()
        fact = data['text']
        print(fact)
    else:
        print('Не вдалося отримати факт про котів.')

def create_cat():
    data = {'name': 'Whiskers', 'color': 'gray', 'breed': 'Persian'}
    response = requests.post('https://api.thecatapi.com/v1/images', json=data)
    if response.status_code == 200:
        cat_data = response.json()
        cat_id = cat_data['id']
        print(f'Створено кота з ID: {cat_id}')
    else:
        print('Не вдалося створити кота.')

def make_api_request(url, method, data=None):
    if method == 'GET':
        response = requests.get(url)
    elif method == 'POST':
        response = requests.post(url, json=data)
    else:
        print('Непідтримуваний метод запиту.')
        return None

    if response.status_code == 200:
        return response.json()
    else:
        print('Помилка при виконанні запиту.')
        return None

# Приклад використання
url = 'https://cat-fact.herokuapp.com/facts/random'
method = 'GET'
response_data = make_api_request(url, method)
if response_data:
    print(response_data)
