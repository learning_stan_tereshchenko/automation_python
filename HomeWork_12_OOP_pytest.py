class Gadget:
    def __init__(self, name, brand, price):
        self.name = name
        self.brand = brand
        self.price = price

    def describe(self):
        print(f"This is a {self.name} made by {self.brand}. It costs {self.price} dollars.")


class Phone(Gadget):
    def __init__(self, name, brand, price, os):
        super().__init__(name, brand, price)
        self.os = os

    def describe(self):
        super().describe()
        print(f"It runs on {self.os}.")


class Laptop(Gadget):
    def __init__(self, name, brand, price, processor):
        super().__init__(name, brand, price)
        self.processor = processor

    def describe(self):
        super().describe()
        print(f"It has a {self.processor} processor.")
import pytest


@pytest.fixture
def phone():
    return Phone("iPhone", "Apple", 999, "iOS")


@pytest.fixture
def laptop():
    return Laptop("MacBook Pro", "Apple", 1499, "Intel Core i7")


def test_gadget(phone, laptop):
    phone.describe()
    laptop.describe()
    assert phone.name == "iPhone"
    assert laptop.brand == "Apple"
    assert phone.price == 999
    assert laptop.processor == "Intel Core i7"


def test_phone(phone):
    phone.describe()
    assert phone.os == "iOS"


def test_laptop(laptop):
    laptop.describe()
    assert laptop.processor == "Intel Core i7"
