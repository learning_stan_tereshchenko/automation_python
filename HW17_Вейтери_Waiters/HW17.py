from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# Установка пути к GeckoDriver
geckodriver_path = "C:\\Users\\PHANTOM\\Desktop\\hillel_домашка\\geckodriver.exe"

# Инициализация веб-драйвера (Firefox) и настройка неявного ожидания
driver = Firefox(executable_path=geckodriver_path)
driver.implicitly_wait(10)  # Установка неявного ожидания в 10 секунд

# Открытие страницы
driver.get("https://demoqa.com/dynamic-properties")

# Ожидание, пока кнопка станет доступной
button1 = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, "enableAfter")))

# Тест 1: Проверка включения кнопки "Will enable 5 seconds"
assert button1.is_enabled(), "Button 'Will enable 5 seconds' is not enabled"

# Тест 2: Проверка изменения цвета текста на кнопке "Color Change"
button2 = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "colorChange")))
initial_color = button2.value_of_css_property("color")

# Проверка изменения цвета (ожидание до момента, когда цвет изменится)
while button2.value_of_css_property("color") == initial_color:
    pass

# Проверка изменения цвета
updated_color = button2.value_of_css_property("color")
assert initial_color != updated_color, "Button 'Color Change' text color did not change"

# Тест 3: Проверка появления кнопки "Visible After 5 Seconds"
button3 = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "visibleAfter")))
assert button3.is_displayed(), "Button 'Visible After 5 Seconds' is not visible"

# Закрытие браузера
driver.quit()
