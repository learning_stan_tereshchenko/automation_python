class Point:
    def __init__(self, x, y):
        self._x = x
        self._y = y

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        if isinstance(value, (int, float)):
            self._x = value
        else:
            raise ValueError('x must be a number')

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        if isinstance(value, (int, float)):
            self._y = value
        else:
            raise ValueError('y must be a number')


class Line:
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def __eq__(self, other):
        if isinstance(other, Line):
            return self.length == other.length
        return False

    def __ne__(self, other):
        return not self == other

    def __lt__(self, other):
        if isinstance(other, Line):
            return self.length < other.length
        return NotImplemented

    def __le__(self, other):
        if isinstance(other, Line):
            return self.length <= other.length
        return NotImplemented

    def __gt__(self, other):
        if isinstance(other, Line):
            return self.length > other.length
        return NotImplemented

    def __ge__(self, other):
        if isinstance(other, Line):
            return self.length >= other.length
        return NotImplemented

    @property
    def length(self):
        dx = self.end.x - self.start.x
        dy = self.end.y - self.start.y
        return math.sqrt(dx ** 2 + dy ** 2)
