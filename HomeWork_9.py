#Створіть клас "Транспортний засіб" та підкласи "Автомобіль", "Літак", "Корабель", наслідувані від "Транспортний засіб".
# Наповніть класи атрибутами на свій розсуд. Створіть обʼєкти класів "Автомобіль", "Літак", "Корабель".
class Transport:
    def __init__(self, name, max_speed, capacity, fuel_type):
        self.name = name
        self.max_speed = max_speed
        self.capacity = capacity
        self.fuel_type = fuel_type

    def move(self):
        print(f"{self.name} is moving")

class Car(Transport):
    def __init__(self, name, max_speed, capacity, fuel_type, num_wheels):
        super().__init__(name, max_speed, capacity, fuel_type)
        self.num_wheels = num_wheels

    def park(self):
        print(f"{self.name} is parked")

class Plane(Transport):
    def __init__(self, name, max_speed, capacity, fuel_type, max_altitude):
        super().__init__(name, max_speed, capacity, fuel_type)
        self.max_altitude = max_altitude

    def takeoff(self):
        print(f"{self.name} is taking off")

    def land(self):
        print(f"{self.name} is landing")

class Ship(Transport):
    def __init__(self, name, max_speed, capacity, fuel_type, length):
        super().__init__(name, max_speed, capacity, fuel_type)
        self.length = length

    def dock(self):
        print(f"{self.name} is docking")

# Створення об'єктів

car1 = Car("Tesla Model S", 250, 5, "Electric", 4)
plane1 = Plane("Boeing 747", 1000, 400, "Jet fuel", 40000)
ship1 = Ship("Queen Mary 2", 35, 2000, "Diesel", 345)

car1.move()  # Tesla Model S is moving
plane1.takeoff()  # Boeing 747 is taking off
ship1.dock()  # Queen Mary 2 is docking
