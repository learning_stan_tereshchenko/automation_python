#Завдання 1
#Напишіть функцію, яка приймає два аргументи.
#a) Якщо обидва аргумени відносяться до числових типів функція пермножує ці аргументи і повертає результат
#b) Якшо обидва аргументи відносяться до типу стрінг функція обʼєднує їх в один і повертає
#c) В будь-якому іншому випадку - функція повертає кортеж з двох агрументів
def multiply_or_concatenate(arg1, arg2):
    if isinstance(arg1, (int, float)) and isinstance(arg2, (int, float)):
        return arg1 * arg2
    elif isinstance(arg1, str) and isinstance(arg2, str):
        return arg1 + arg2
    else:
        return arg1, arg2
#Завдання 2
#Візьміть попереднє дз "Касир в кінотеатрі" і перепишіть за допомогою функцій.
def ask_age():
  age = input("Будь ласка, введіть ваш вік: ")
  return int(age)

def check_age(age):
  if age < 7:
    return "Де твої батьки?"
  elif age < 16:
    return "Це фільм для дорослих!"
  elif age > 65:
    return "Покажіть пенсійне посвідчення!"
  elif "7" in str(age):
    return "Вам сьогодні пощастить!"
  else:
    return "А білетів вже немає!"

def main():
  try:
    age = ask_age()
    result = check_age(age)
    print(result)
  except ValueError:
    print("Невірний формат віку.")

if __name__ == "__main__":
  main()
