1) Создание виртуального окружения:
python3.11 -m venv venv
2) Активация виртуального окружения:
Для Windows:
venv\Scripts\activate.bat
Для Linux/Mac:
source venv/bin/activate
3) Установка зависимостей:
pip install -r requirements.txt
4) Запуск всех тестов с помощью pytest:
pytest
5) Запуск тестов, отмеченных маркировкой from_class:
pytest -m from_class
6) Запуск тестов с параметризацией:
pytest param_tests.py
В данном случае будут запущены все тесты из файла param_tests.py, включая параметризованные тесты.

7) Запуск только одного теста с параметром:
pytest param_tests.py::test_single_param[1]
В данном случае будет запущен тест test_single_param с параметром 1.

8) Запуск только тестов, отмеченных маркировкой from_class:
pytest -m from_class
В данном случае будут запущены только тесты, отмеченные маркировкой from_class.

9) Деактивация виртуального окружения:
deactivate.bat