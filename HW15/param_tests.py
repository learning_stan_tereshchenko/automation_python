import pytest

@pytest.fixture(params=[1, 2, 3])
def param(request):
    return request.param

def test_single_param(param):
    assert param in [1, 2, 3]

def test_single_param(param):
    assert isinstance(param, int)

@pytest.mark.parametrize("param", [1, 2, 3])
def test_multiple_params(param):
    assert isinstance(param, int)

@pytest.mark.parametrize("param", [(1, 'one'), (2, 'two'), (3, 'three')], ids=["one", "two", "three"])
def test_param_with_alias(param):
    assert isinstance(param, tuple)
