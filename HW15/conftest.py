import pytest


@pytest.fixture(scope="session", autouse=True)
def setup_session():
    print("\nStarting tests...")
    yield
    print("\nTests finished.")


@pytest.fixture(autouse=True)
def setup():
    print("\nStarting test...")
    yield
    print("\nTest finished.")
