from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.remote.webdriver import WebDriver


class CheckboxWidget:
    def __init__(self, driver: WebDriver):
        self.driver = driver

    def select_checkboxes(self, *checkbox_names):
        for checkbox_name in checkbox_names:
            checkbox = self.driver.find_element(By.XPATH, f"//span[text()='{checkbox_name}']/preceding-sibling::span")
            checkbox.click()


class RadioButtonWidget:
    def __init__(self, driver: WebDriver):
        self.driver = driver

    def activate_radio_button(self, button_name):
        radio_button = self.driver.find_element(By.XPATH, f"//label[text()='{button_name}']/input")
        radio_button.click()

    def get_radio_button_info(self):
        radio_buttons = self.driver.find_elements(By.CSS_SELECTOR, "input[type='radio']")
        info_dict = {}
        for button in radio_buttons:
            button_name = button.find_element(By.XPATH, "..//following-sibling::label").text
            is_enabled = button.is_enabled()
            is_selected = button.is_selected()
            info_dict[button_name] = {"enabled": is_enabled, "selected": is_selected}
        return info_dict


class CheckboxTest:
    def test_select_checkboxes(self, checkbox_widget):
        checkbox_widget.select_checkboxes("Commands (Home-Desktop)", "General (Home-Documents-Office)")


class RadioButtonTest:
    def test_activate_yes_radio(self, radio_button_widget):
        radio_button_widget.activate_radio_button("Yes")
        assert radio_button_widget.get_radio_button_info()["Yes"]["selected"]

    def test_get_radio_buttons_info(self, radio_button_widget):
        info_dict = radio_button_widget.get_radio_button_info()
        assert len(info_dict) == 2
        assert "Yes" in info_dict
        assert "No" in info_dict

    def test_activate_disabled_radio_button(self, radio_button_widget):
        radio_button_widget.activate_radio_button("No")
        assert radio_button_widget.get_radio_button_info()["No"]["selected"]


# инициализация браузера и создание экземпляров виджетов
driver = WebDriver.Chrome()
checkbox_widget = CheckboxWidget(driver)
radio_button_widget = RadioButtonWidget(driver)

# создание экземпляров тестов и передача виджетов в качестве аргументов
checkbox_test = CheckboxTest()
radio_button_test = RadioButtonTest()

# выполнение тестов
checkbox_test.test_select_checkboxes(checkbox_widget)
radio_button_test.test_activate_yes_radio(radio_button_widget)
radio_button_test.test_get_radio_buttons_info(radio_button_widget)
radio_button_test.test_activate_disabled_radio_button(radio_button_widget)

# закрытие браузера
driver.quit()
