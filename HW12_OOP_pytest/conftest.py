import pytest
from gadget import Phone, Laptop

@pytest.fixture
def phone():
    return Phone('Apple', 'iPhone 13', 1000, 6.1)

@pytest.fixture
def laptop():
    return Laptop('Apple', 'MacBook Pro', 2000, 2.0)
