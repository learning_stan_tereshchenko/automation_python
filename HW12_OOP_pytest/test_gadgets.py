from gadget import Phone, Laptop


def test_phone_calculate_price(phone):
    assert phone.calculate_price() == 1100


def test_laptop_calculate_price(laptop):
    assert laptop.calculate_price() == 2400
