from abc import ABC, abstractmethod

class Gadget(ABC):
    def __init__(self, name, brand, price):
        self.name = name
        self.brand = brand
        self.price = price

    @abstractmethod
    def get_specs(self):
        pass

class Phone(Gadget):
    def __init__(self, name, brand, price, os, screen_size):
        super().__init__(name, brand, price)
        self.os = os
        self.screen_size = screen_size

    def get_specs(self):
        return f"{self.brand} {self.name}, OS: {self.os}, Screen size: {self.screen_size}, Price: {self.price}"

class Laptop(Gadget):
    def __init__(self, name, brand, price, os, screen_size, processor):
        super().__init__(name, brand, price)
        self.os = os
        self.screen_size = screen_size
        self.processor = processor

    def get_specs(self):
        return f"{self.brand} {self.name}, OS: {self.os}, Screen size: {self.screen_size}, Processor: {self.processor}, Price: {self.price}"
