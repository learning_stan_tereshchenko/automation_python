import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager

# Инициализация драйвера Chrome
driver = webdriver.Chrome(ChromeDriverManager().install())
driver.maximize_window()

# Переход на страницу https://www.searates.com/
driver.get("https://www.searates.com/")

# Клик на вкладку "Routes"
driver.find_element(By.CSS_SELECTOR, ".main-nav-links [href='/routes/']").click()

# Ввод города отправления
departure_input = driver.find_element(By.NAME, "searchFrom")
departure_input.clear()
departure_input.send_keys("Shanghai")
time.sleep(1)
departure_input.send_keys(Keys.RETURN)

# Ввод города прибытия
arrival_input = driver.find_element(By.NAME, "searchTo")
arrival_input.clear()
arrival_input.send_keys("Rotterdam")
time.sleep(1)
arrival_input.send_keys(Keys.RETURN)

# Выбор типа груза
cargo_type_dropdown = driver.find_element(By.ID, "searchForm_cargoType")
cargo_type_dropdown.click()
time.sleep(1)
cargo_type_option = driver.find_element(By.XPATH, "//option[contains(text(),'Container (20')]")
cargo_type_option.click()

# Нажатие кнопки "Search"
search_button = driver.find_element(By.CSS_SELECTOR, "#searchForm button[type='submit']")
search_button.click()

# Ожидание загрузки страницы с результатами поиска
wait = WebDriverWait(driver, 10)
wait.until(EC.url_contains("/routes/"))

# Закрытие браузера
driver.quit()
